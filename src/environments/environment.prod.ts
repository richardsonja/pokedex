export const environment = {
  production: true,
  applicationName: 'Pokedex',
  backendUrl: 'api/pokemons/',
};
