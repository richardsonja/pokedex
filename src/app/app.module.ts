import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {CoreModule} from './core/core.module';
import {HttpClientModule} from '@angular/common/http';
import {MaterialModule} from './core/material.module';

import {FlexLayoutModule} from '@angular/flex-layout';
import {SharedModule} from './shared/shared.module';
import {ReactiveFormsModule} from '@angular/forms';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {environment} from '../environments/environment';
import {StoreRouterConnectingModule} from '@ngrx/router-store';
import {EffectsModule} from '@ngrx/effects';
import {StoreModule} from '@ngrx/store';
import {appReducers} from './store/app.reducers';
import {SnackbarEffects} from './store/snackbar/snackbar.effects';
import {HttpErrorEffects} from './store/httperror/http-error.effects';
import {ErrorFacade} from './store/httperror/http-error.facade';
import {SnackbarFacade} from './store/snackbar/snackbar.facade';
import {DashboardModule} from "./modules/dashboard/dashboard.module";
import {InMemoryDataService} from "./core/services/in-memory-data.service";
import {HttpClientInMemoryWebApiModule} from "angular-in-memory-web-api";
import {PokemonEffects} from "./store/pokemon/pokemon.effects";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    SharedModule,
    CoreModule,
    MaterialModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    DashboardModule,

    HttpClientModule,
    // this will simulate a api but will keep it local.
    HttpClientInMemoryWebApiModule.forRoot(InMemoryDataService),
    // store related
    StoreModule.forRoot(appReducers),
    EffectsModule.forRoot([
      SnackbarEffects,
      HttpErrorEffects,
      PokemonEffects,
    ]),
    StoreRouterConnectingModule.forRoot({stateKey: 'router'}),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    AppRoutingModule // must be imported as the last module as it contains the fallback route
  ],
  providers: [
    ErrorFacade,
    SnackbarFacade,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
