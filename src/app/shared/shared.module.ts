import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FlexLayoutModule} from '@angular/flex-layout';
import {HeaderComponent} from './components/header/header.component';
import {MaterialModule} from '../core/material.module';
import {LayoutComponent} from './components/layout/layout.component';
import {RouterModule} from '@angular/router';
import {ConfirmationDialogComponent} from './components/confirmation-dialog/confirmation-dialog.component';
import {FooterComponent} from "./components/footer/footer.component";

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    FlexLayoutModule,
    RouterModule
  ],
  providers: [],
  declarations: [
    HeaderComponent,
    FooterComponent,
    LayoutComponent,
    ConfirmationDialogComponent
  ],
  exports: [
    HeaderComponent,
    FooterComponent,
    LayoutComponent,
    ConfirmationDialogComponent
  ],
  entryComponents: [
    ConfirmationDialogComponent
  ]
})
export class SharedModule {
}
