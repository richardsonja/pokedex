import {RouterReducerState} from '@ngrx/router-store';

import {initialSnackbarState, ISnackbarState} from './snackbar/snackbar.state';
import {initialPokemonState, IPokemonState} from "./pokemon/pokemon.state";

export interface IAppState {
  router?: RouterReducerState;
  snackbar: ISnackbarState;
  pokemon: IPokemonState;
}

export const initialAppState: IAppState = {
  snackbar: initialSnackbarState,
  pokemon: initialPokemonState
};

export function getInitialState(): IAppState {
  return initialAppState;
}
