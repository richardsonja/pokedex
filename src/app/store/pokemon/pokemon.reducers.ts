import {Action, createReducer, on} from '@ngrx/store';
import {initialPokemonState, IPokemonState} from "./pokemon.state";
import {addPokemonSuccess, deletePokemonSuccess, getPokemonsSuccess, updatePokemonSuccess} from "./pokemon.actions";

function arrayToObject(array) {
  return array.reduce((obj, item) => {
    obj[item.id] = item;
    return obj;
  }, {});
}

export const reducer = createReducer(
  initialPokemonState,
  on(getPokemonsSuccess, (state, {pokemons}) => {
    return ({
      ...state,
      pokemonEntities: arrayToObject(pokemons)
    });
  }),
  on(addPokemonSuccess, (state, {pokemon}) => {
    return ({
      ...state.pokemonEntities,
      [pokemon.id]: pokemon
    });
  }),
  on(deletePokemonSuccess, (state, {id}) => {
    const entities = {...state.pokemonEntities};
    delete entities[id];
    return ({
      ...state,
      pokemonEntities: entities
    });
  }),
  on(updatePokemonSuccess, (state, {pokemon}) => {
    return ({
      ...state.pokemonEntities,
      [pokemon.id]: pokemon
    });
  }),
);

export function pokemonReducers(
  state: IPokemonState | undefined,
  action: Action) {
  return reducer(state, action);
}
