import {createAction, props, union} from '@ngrx/store';
import {Pokemon} from "../../core/models/pokemon.model";
import {HttpError} from "../../core/models/http_error.model";

export const getPokemons = createAction('[Pokemon] Load pokemon');
export const getPokemonsSuccess = createAction('[Pokemon] Load pokemon success',
  props<{ pokemons: Pokemon[] }>());
export const getPokemonsFailed = createAction('[Pokemon] Load pokemon failed',
  props<{ httpError: HttpError }>());

export const addPokemon = createAction('[Pokemon] Add',
  props<{ pokemon: Pokemon }>());
export const addPokemonSuccess = createAction('[Pokemon] Add success',
  props<{ pokemon: Pokemon }>());
export const addPokemonFailed = createAction('[Pokemon] Add failed',
  props<{ httpError: HttpError }>());

export const deletePokemon = createAction('[Pokemon] Delete',
  props<{ id: number }>());
export const deletePokemonSuccess = createAction('[Pokemon] Delete success',
  props<{ id: number }>());
export const deletePokemonFailed = createAction('[Pokemon] Delete failed',
  props<{ httpError: HttpError }>());


export const updatePokemon = createAction('[Pokemon] Update',
  props<{ pokemon: Pokemon }>());
export const updatePokemonSuccess = createAction('[Pokemon] Update success',
  props<{ pokemon: Pokemon }>());
export const updatePokemonFailed = createAction('[Pokemon] Update failed',
  props<{ httpError: HttpError }>());


const pokemonActions = union({
  getPokemons,
  getPokemonsSuccess,
  getPokemonsFailed,
  addPokemon,
  addPokemonSuccess,
  addPokemonFailed,
  deletePokemon,
  deletePokemonSuccess,
  deletePokemonFailed,
  updatePokemon,
  updatePokemonSuccess,
  updatePokemonFailed,
});

export type pokemonActionsUnion = typeof pokemonActions;
