import {Pokemon} from "../../core/models/pokemon.model";

export interface IPokemonState {
  ids: number[];
  pokemonEntities: { [key: string]: Pokemon };
}

export const initialPokemonState: IPokemonState = {
  ids: [],
  pokemonEntities: {}
};
