import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {catchError, map, switchMap, tap} from 'rxjs/operators';
import {
  addPokemon,
  addPokemonFailed,
  addPokemonSuccess,
  deletePokemon,
  deletePokemonFailed,
  deletePokemonSuccess,
  getPokemons,
  getPokemonsFailed,
  getPokemonsSuccess,
  pokemonActionsUnion,
  updatePokemon,
  updatePokemonFailed,
  updatePokemonSuccess
} from "./pokemon.actions";
import {of} from "rxjs";
import {PokemonService} from "../../core/services/pokemon.service";
import {Pokemon} from "../../core/models/pokemon.model";
import {recordHttpError} from "../httperror/http-error.actions";
import {MatSnackBar} from "@angular/material/snack-bar";
import {snackbarOpen} from "../snackbar/snackbar.actions";

@Injectable()
export class PokemonEffects {

  getPokemons$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getPokemons.type),
      switchMap(() =>
        this.pokemonService.getAll()
          .pipe(
            map(pokemons => getPokemonsSuccess({pokemons})),
            catchError((ex) => of(getPokemonsFailed({httpError: {httpErrorResponse: ex}})))
          )
      ),
      catchError((ex) => of(getPokemonsFailed({httpError: {httpErrorResponse: ex}})))
    ));

  addPokemon$ = createEffect(() =>
    this.actions$.pipe(
      ofType(addPokemon.type),
      map(action => action.pokemon),
      switchMap((pokemon: Pokemon) =>
        this.pokemonService.add(pokemon).pipe(
          map(pokemon => addPokemonSuccess({pokemon})),
          catchError((ex) => of(addPokemonFailed({httpError: {httpErrorResponse: ex}})))
        )
      ),
      catchError((ex) => of(addPokemonFailed({httpError: {httpErrorResponse: ex}})))
    ));

  deletePokemon$ = createEffect(() =>
    this.actions$.pipe(
      ofType(deletePokemon.type),
      map(action => action.id),
      switchMap((id: number) =>
        this.pokemonService.delete(id).pipe(
          map(() => deletePokemonSuccess({id})),
          catchError((ex) => of(deletePokemonFailed({httpError: {httpErrorResponse: ex}})))
        )
      ),
      catchError((ex) => of(deletePokemonFailed({httpError: {httpErrorResponse: ex}})))
    )
  );

  updatePokemon$ = createEffect(() =>
    this.actions$.pipe(
      ofType(updatePokemon.type),
      map(action => action.pokemon),
      switchMap((pokemon: Pokemon) =>
        this.pokemonService.update(pokemon).pipe(
          map(pokemon => updatePokemonSuccess({pokemon})),
          catchError((ex) => of(updatePokemonFailed({httpError: {httpErrorResponse: ex}})))
        )
      ),
      catchError((ex) => of(updatePokemonFailed({httpError: {httpErrorResponse: ex}})))
    ));

  pokemonCitySuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(
        getPokemonsSuccess.type,
        addPokemonSuccess.type,
        deletePokemonSuccess.type,
        updatePokemonSuccess.type
      ),
      tap(() =>
        of(snackbarOpen({
          message: 'SUCCESS',
          action: 'Operation Successful',
          config: {
            duration: 2000
          }
        }))
      )
    ));

  pokemonCityFailed$ = createEffect(() =>
    this.actions$.pipe(
      ofType(
        getPokemonsFailed.type,
        addPokemonFailed.type,
        deletePokemonFailed.type,
        updatePokemonFailed.type
      ),
      map(action => action.httpError),
      switchMap(httpError => of(recordHttpError({httpError})))
    ));

  constructor(
    private pokemonService: PokemonService,
    private actions$: Actions<pokemonActionsUnion>,
    private snackBar: MatSnackBar,
  ) {
  }
}
