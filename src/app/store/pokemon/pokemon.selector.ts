import {createFeatureSelector, createSelector} from '@ngrx/store';
import {IPokemonState} from "./pokemon.state";


export const selectPokemonState = createFeatureSelector<IPokemonState>(
  'pokemon'
);

export const selectPokemonList = createSelector(
  selectPokemonState,
  state => Object.values(state.pokemonEntities)
);
