import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import {IAppState} from '../app.state';
import {selectPokemonList} from "./pokemon.selector";
import {addPokemon, deletePokemon, getPokemons, updatePokemon} from "./pokemon.actions";
import {Pokemon} from "../../core/models/pokemon.model";

@Injectable()
export class PokemonFacade {
  allPokemons$ = this.store.select(selectPokemonList);

  constructor(private store: Store<IAppState>) {
  }

  loadAll() {
    this.store.dispatch(getPokemons());
  }

  add(pokemon: Pokemon) {
    this.store.dispatch(addPokemon({pokemon}));
  }

  delete(id: number) {
    this.store.dispatch(deletePokemon({id}));
  }

  update(pokemon: Pokemon) {
    this.store.dispatch(updatePokemon({pokemon}));
  }
}
