import {initialSnackbarState, ISnackbarState} from './snackbar.state';
import {Action, createReducer, on} from '@ngrx/store';
import {snackbarClose, snackbarOpen} from './snackbar.actions';

export const reducer = createReducer(
  initialSnackbarState,
  on(snackbarOpen, (state) => ({
    ...state,
    show: true
  })),
  on(snackbarClose, (state) => ({
    ...state,
    show: false
  }))
);

export function snackbarReducers(
  state: ISnackbarState | undefined,
  action: Action) {
  return reducer(state, action);
}
