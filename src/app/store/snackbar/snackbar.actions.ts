import {createAction, props, union} from '@ngrx/store';
import {MatSnackBarConfig} from '@angular/material';

export const snackbarClose = createAction('[Snackbar] Close');
export const snackbarOpen = createAction('[Snackbar] Open', props<{
  message: string,
  action?: string,
  config?: MatSnackBarConfig
}>());

const snackbarActions = union({
  snackbarOpen,
  snackbarClose
});

export type snackbarActionsUnion = typeof snackbarActions;
