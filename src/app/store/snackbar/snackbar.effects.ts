import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {delay, map, tap} from 'rxjs/operators';
import {snackbarActionsUnion, snackbarClose, snackbarOpen} from './snackbar.actions';
import {MatSnackBar} from '@angular/material';

@Injectable()
export class SnackbarEffects {

  snackbarClose$ = createEffect(() =>
    this.actions$.pipe(
      ofType(snackbarClose.type),
      tap(() => this.matSnackBar.dismiss())
    ), {
    dispatch: false
  });

  snackbarOpen$ = createEffect(() =>
    this.actions$.pipe(
      ofType(snackbarOpen.type),
      tap(payload => console.log(payload.message, payload.action, payload.config)),
      tap(payload => this.matSnackBar.open(payload.message, payload.action, payload.config)),
      delay(10000),
      map(() => snackbarClose())
    ));

  constructor(
    private matSnackBar: MatSnackBar,
    private actions$: Actions<snackbarActionsUnion>
  ) {
  }
}
