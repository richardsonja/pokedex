import {ActionReducerMap} from '@ngrx/store';

import {routerReducer} from '@ngrx/router-store';
import {IAppState} from './app.state';
import {snackbarReducers} from './snackbar/snackbar.reducers';
import {pokemonReducers} from "./pokemon/pokemon.reducers";

export const appReducers: ActionReducerMap<IAppState, any> = {
  router: routerReducer,
  snackbar: snackbarReducers,
  pokemon: pokemonReducers
};
