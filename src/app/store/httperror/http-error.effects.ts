import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {httpErrorActionsUnion, recordHttpError} from './http-error.actions';
import {map, switchMap, tap} from 'rxjs/operators';
import {snackbarOpen} from '../snackbar/snackbar.actions';
import {EventLoggingService} from '../../core/services/utilities/event_logging.service';
import {of} from 'rxjs';

@Injectable()
export class HttpErrorEffects {
  recordHttpError$ = createEffect(() =>
    this.actions$.pipe(
      ofType(recordHttpError.type),
      tap(action => {
        if (action.httpError.httpErrorResponse != null && action.httpError.httpErrorResponse.message != null) {
          this.eventLoggingService.logHttpErrorResponse(action.type, action.httpError.httpErrorResponse, 'action');
        }
      }),
      tap(action => {
        if (action.httpError.message) {
          this.eventLoggingService.logError(action.type, action.httpError.message, 'action');
        }
      }),
      map(action => {
        const errorMessage = action.httpError.message;
        const exception = action.httpError.httpErrorResponse;
        const message: string = errorMessage ? errorMessage : '';

        if (exception === undefined || exception == null) {
          return message;
        }

        if (exception.status !== undefined && exception.status === 0) {
          return message
            ? message + '....Also, this is embarrassing, we are having an issue.'
            : `This is embarrassing, we're having an issue.`;
        }

        return message + '; ' + exception.message;
      }),
      switchMap(errorMessage => {
        return of(snackbarOpen({message: errorMessage}));
      })
    ));

  constructor(
    private eventLoggingService: EventLoggingService,
    private actions$: Actions<httpErrorActionsUnion>
  ) {
  }
}
