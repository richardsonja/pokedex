import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import {IAppState} from '../app.state';
import {recordHttpError} from './http-error.actions';
import {HttpErrorResponse} from '@angular/common/http';

@Injectable()
export class ErrorFacade {

  constructor(private store: Store<IAppState>) {
  }

  recordHttpErrorResponse(httpErrorResponse: HttpErrorResponse) {
    this.store.dispatch(recordHttpError({httpError: {httpErrorResponse}}));
  }

  recordMessageAndHttpErrorResponse(message: string, httpErrorResponse: HttpErrorResponse) {
    this.store.dispatch(recordHttpError({httpError: {httpErrorResponse, message}}));
  }

  recordMessage(message: string) {
    this.store.dispatch(recordHttpError({httpError: {message}}));
  }
}
