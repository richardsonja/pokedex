import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DashboardRoutingModule} from './dashboard-routing.module';
import {MaterialModule} from '../../core/material.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {ReactiveFormsModule} from '@angular/forms';
import {DashboardComponent} from './dashboard.component';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    DashboardRoutingModule
  ],
  exports: [],
  providers: [],
  declarations: [
    DashboardComponent,
  ]
})

export class DashboardModule {
}
