import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Pokemon} from "../models/pokemon.model";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class PokemonService {
  private url: string = environment.backendUrl;

  constructor(private http: HttpClient) {
  }

  public getAll(): Observable<Pokemon[]> {
    return this.http.get<Pokemon[]>(this.url);
  }

  public delete(id: number): Observable<Pokemon> {
    return this.http.delete<Pokemon>(`${this.url}/${id}`);
  }

  public add(pokemon: Pokemon): Observable<Pokemon> {
    return this.http.post<Pokemon>(this.url, pokemon);
  }

  public update(pokemon: Partial<Pokemon>): Observable<Pokemon> {
    return this.http.put<Pokemon>(`${this.url}`, pokemon);
  }
}
