import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {Observable, of} from 'rxjs';


@Injectable()
export class EnvironmentService {

  get(): Observable<any> {
    return of(environment);
  }
}
