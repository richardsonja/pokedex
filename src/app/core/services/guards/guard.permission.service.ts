import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';

@Injectable()
export class PermissionGuard implements CanActivate {
  constructor(
    private router: Router
  ) {
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    const permission = route.data['permission'];
    const redirectRoute = route.data['redirectTo'];
    const allowed = true;

    if (!allowed && redirectRoute) {
      this.router.navigate(redirectRoute);
    }

    return allowed;
  }
}
