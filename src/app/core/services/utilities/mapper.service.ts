import {Injectable} from '@angular/core';
import {JsonConvert, OperationMode, ValueCheckingMode} from 'json2typescript';
import {EnvironmentService} from '../environment.service';
import {BaseService} from '../infrastructure/base.service';
import {takeUntil} from 'rxjs/operators';


@Injectable()
export class MapperService extends BaseService {
  private operationMode: OperationMode = null;

  constructor(private environmentService: EnvironmentService) {
    super();
    this.environmentService.get()
      .pipe(takeUntil(this.unsubscribe))
      .subscribe(x =>
        this.operationMode = x.serializerDebugMode
          ? OperationMode.LOGGING
          : OperationMode.DISABLE);
  }

  public serializeArray(values: any[]): any[] {
    const jsonConvert: JsonConvert = new JsonConvert();
    return jsonConvert.serializeArray(values);
  }

  public deserializeArray<T>(Clazz: new() => T, jsonArray: any[]): T[] {
    const jsonConvert: JsonConvert = new JsonConvert();
    jsonConvert.operationMode = this.operationMode; // print some debug data
    jsonConvert.ignorePrimitiveChecks = false; // don't allow assigning number to string etc.
    jsonConvert.valueCheckingMode = ValueCheckingMode.DISALLOW_NULL; // never allow null

    return jsonConvert.deserializeArray(jsonArray, Clazz);
  }

  public deserializeObject<T>(Clazz: new() => T, json: any): T {
    const jsonConvert: JsonConvert = new JsonConvert();
    jsonConvert.operationMode = this.operationMode; // print some debug data
    jsonConvert.ignorePrimitiveChecks = false; // don't allow assigning number to string etc.
    jsonConvert.valueCheckingMode = ValueCheckingMode.ALLOW_NULL; // never allow null

    return jsonConvert.deserializeObject(json, Clazz);
  }
}
