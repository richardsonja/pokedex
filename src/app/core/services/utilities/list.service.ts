import {Injectable} from '@angular/core';
import {ClazzService} from './clazz.service';

@Injectable()
export class ListService {
  constructor(private clazzService: ClazzService) {
  }

  orderBy(values: any[], ascendingOrder: boolean = true, orderByValue?: string): any[] {
    if (!values) {
      return values;
    }

    const cloneValues = Array.from(values);
    const orderByValuePath = this.clazzService.splitString(orderByValue);

    const sortedValues: any[] = cloneValues
      .sort((n1, n2) => {
        const current = this.getPropertyByPath(n1, orderByValuePath, !orderByValue);
        const next = this.getPropertyByPath(n2, orderByValuePath, !orderByValue);

        return this.clazzService.naturalSort(current, next);
      });

    return ascendingOrder
      ? sortedValues
      : sortedValues.reverse();
  }

  shuffle(values: any[]) {
    // Fisher–Yates Shuffle
    if (!values) {
      return values;
    }

    const cloneValues = Array.from(values);
    let maxNumberOfElements = cloneValues.length;
    let element;
    let index;

    // While there remain elements to shuffle…
    while (maxNumberOfElements) {
      // Pick a remaining element…
      index = Math.floor(Math.random() * maxNumberOfElements--);

      // And swap it with the current element.
      element = cloneValues[maxNumberOfElements];
      cloneValues[maxNumberOfElements] = cloneValues[index];
      cloneValues[index] = element;
    }

    return cloneValues;
  }

  firstOrDefault(values: any[]): any {
    return !values ? values : Array.from(values).shift();
  }

  lastOrDefault(values: any[]): any {
    return !values ? values : Array.from(values).pop();
  }

  private getPropertyByPath(clazz: any, properties: string[], useWholeClazz: boolean): any {
    return useWholeClazz
      ? this.clazzService.toLowerCaseString(clazz)
      : this.clazzService.toLowerCaseString(this.clazzService.getProperty(clazz, properties));
  }
}
