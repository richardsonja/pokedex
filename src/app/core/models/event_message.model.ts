export class EventMessage {
  applicationName: string;
  routePath: string;
  message: string;
  userAgent: string;
  userId: string;
}
