import {ErrorHandler, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {HttpTokenInterceptor} from './interceptors/http.token.interceptor';

import {ApiService} from './services/infrastructure/api.service';
import {TokenService} from './services/infrastructure/token.service';
import {AuthGuard} from './services/guards/guard.auth.service';
import {SharedModule} from '../shared/shared.module';
import {ListService} from './services/utilities/list.service';
import {StorageService} from './services/infrastructure/storage.service';
import {MapperService} from './services/utilities/mapper.service';
import {EnvironmentService} from './services/environment.service';
import {ClazzService} from './services/utilities/clazz.service';
import {CustomErrorHandler} from './interceptors/custom-error.handler';
import {EventLoggingService} from './services/utilities/event_logging.service';
import {HttpUnavailableInterceptor} from './interceptors/http.unavailable.interceptor';
import {PermissionGuard} from './services/guards/guard.permission.service';


@NgModule({
  imports: [
    CommonModule,
    SharedModule
  ],
  providers: [
    [
      {
        provide: ErrorHandler,
        useClass: CustomErrorHandler
      }
    ],
    [
      {
        provide: HTTP_INTERCEPTORS,
        useClass: HttpTokenInterceptor,
        multi: true
      },
      {
        provide: HTTP_INTERCEPTORS,
        useClass: HttpUnavailableInterceptor,
        multi: true
      }
    ],
    // utility, guards
    ApiService,
    AuthGuard,
    PermissionGuard,
    EnvironmentService,
    ListService,
    ClazzService,
    MapperService,
    EventLoggingService,

    // services
    TokenService,
    StorageService,
  ],
  declarations: []
})
export class CoreModule {
}
