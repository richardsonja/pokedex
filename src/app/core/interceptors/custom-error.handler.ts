import {ErrorHandler, Injectable, Injector} from '@angular/core';
import {EventLoggingService} from '../services/utilities/event_logging.service';
import {LocationStrategy, PathLocationStrategy} from '@angular/common';

@Injectable()
export class CustomErrorHandler implements ErrorHandler {
  constructor(private injector: Injector) {
  }

  handleError(error: any): void {
    const userId = this.getUserId();
    const routePath = this.getRoutePath();

    const eventLoggingService = this.injector.get(EventLoggingService);
    const errorMessage = eventLoggingService.parseErrorMessage(error);

    eventLoggingService.logError(routePath, errorMessage, userId);

    throw error;
  }

  private parseErrorMessage(error: any): string {
    return error.message ? error.message : error.toString();
  }

  private getRoutePath(): string {
    const locationService = this.injector.get(LocationStrategy);
    return locationService instanceof PathLocationStrategy ? locationService.path() : '/';
  }

  private getUserId(): string {
    return 'TBD player';
  }
}
